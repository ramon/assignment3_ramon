import React, { Component } from 'react'

class Form extends Component {
   
    constructor(props) {
        super(props)
    
        this.state = {
            id:"",
            title:"",
            director:"",
            year:""
        }
    }
    
    handleIdChange = (e) =>{
        this.setState({
            id: e.target.value
        })
    }

    handleTitleChange = (e) =>{
        this.setState({
            title: e.target.value
        })
    }

    handleDirectorChange = (e) =>{
        this.setState({
            director: e.target.value
        })
    }

    handleYearChange = (e) =>{
        this.setState({
            year: e.target.value
        })
    }

    render() {
        return (
            <form>
                <div>
                    <label>id</label>
                    <input type='text' value={this.state.id} onChange={this.handleIdChange}/>
                    <label>title</label>
                    <input type='text' value={this.state.title} onChange={this.handleTitleChange}/>
                    <label>director</label>
                    <input type='text' value={this.state.director} onChange={this.handleDirectorChange}/>
                    <label>year</label>
                    <input type='text' value={this.state.year} onChange={this.handleYearChange}/>
                </div>
            </form>
        )
    }
}

export default Form
