import React, { Component } from 'react'
import axios from 'axios'
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';


const defaultColDef={
        floatingFilter:true
    }

const columnDefs=[
    { headerName: "id", field: "id" },
    { headerName: "title", field: "title",}, 
    { headerName: "director", field: "director",},
    { headerName: "year", field: "year" }
]

class Table extends Component {
    constructor(props){
        super(props);
        this.apiUrl = "https://lo0h6acpze.execute-api.eu-west-3.amazonaws.com/movies";
        this.state = {
             id:"",
             title:"",
             director:"",
             year:""
        }
    }
    
    onGridReady=(params)=>{
        fetch("https://lo0h6acpze.execute-api.eu-west-3.amazonaws.com/movies").then(resp=>resp.json())
        .then(resp => params.api.applyTransaction({add:resp.Items}))
        this.api = params.api
          }

    //buttons handlers
    handleClickDeleteMovie = event => {
        const newurl = this.apiUrl + "/" + this.state.id
        axios.delete(newurl,{mode:'cors'})
        .then(resp => {console.log(resp)
        })
        event.preventDefault()
    }

    handleClickInsertMovie = (event) => {
        const id = this.state.id
        const title = this.state.title
        const director = this.state.director
        const year = this.state.year
        const data = {id, title, director, year}
        const data2 = JSON.stringify(data)
        console.log(data2)
        axios.put(this.apiUrl,data)
        .catch((error) => {
            console.log(error)
        })
        event.preventDefault()
    }

    handleClickRefresh = (event) => {
        window.location.reload()
    }

    //form handlers
    handleIdChange = (e) =>{
        this.setState({
            id: e.target.value
        })
    }

    handleTitleChange = (e) =>{
        this.setState({
            title: e.target.value
        })
    }

    handleDirectorChange = (e) =>{
        this.setState({
            director: e.target.value
        })
    }

    handleYearChange = (e) =>{
        this.setState({
            year: e.target.value
        })
    }

    render() {
        return (
            <div>
                <div className="App">
                    <h1 align="center">Movies DB</h1>
                    <h3>PLEASE REFRESH DATABASE AFTER EACH ACTION</h3>
                    <button onClick={this.handleClickRefresh}>REFRESH DB</button>
                    <div className="ag-theme-alpine" style={ {height: '300px'} }>
                        <AgGridReact
                            columnDefs={columnDefs}
                            onGridReady={this.onGridReady}
                            defaultColDef={defaultColDef}
                            >
                        </AgGridReact>
                    </div>
                </div>
            <form>
                <div>
                    <label>id</label>
                    <input type='text' value={this.state.id} onChange={this.handleIdChange}/>
                    <label>title</label>
                    <input type='text' value={this.state.title} onChange={this.handleTitleChange}/>
                    <label>director</label>
                    <input type='text' value={this.state.director} onChange={this.handleDirectorChange}/>
                    <label>year</label>
                    <input type='text' value={this.state.year} onChange={this.handleYearChange}/>

                    <button onClick={this.handleClickInsertMovie}>Insert</button>

                    <div>
                    <p>To delete just enter an id and click delete</p>
                    <button onClick={this.handleClickDeleteMovie}>Delete</button>
                    </div>
                </div>
            </form>
            </div>
        )
    }
}

export default Table
