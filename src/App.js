import './App.css';
import Table from './components/Table';
// import Form from './components/Form';

function App() {
  return (
    <div className="App">
      <Table/>
    </div>
  );
}

export default App;
